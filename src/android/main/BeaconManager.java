package be.wearenexen.cordova;

import android.app.Application;
import android.support.annotation.NonNull;

import be.wearenexen.NexenNotificationBuilder;
import be.wearenexen.NexenProximityManager;
import be.wearenexen.NexenProximitySetup;
import be.wearenexen.NexenServiceInterface;
import be.wearenexen.cordova.options.Options;

public final class BeaconManager {

    private static BeaconManager sInstance;

    private NexenProximityManager mProximityManager;
    private Options mOptions;
    private boolean mHasBeenBootstrapped;

    private BeaconManager() {

    }

    public static BeaconManager getInstance() {
        if (sInstance == null) {
            sInstance = new BeaconManager();
        }
        return sInstance;
    }

    public synchronized void start() {
        NexenProximityManager.get().start();
    }

    public synchronized void stop() {
        NexenProximityManager.get().stop();
    }

    public synchronized boolean hasBeenBootstrapped() {
        return mHasBeenBootstrapped;
    }

    public synchronized NexenProximityManager getProximityManager() {
        return mProximityManager;
    }

    public synchronized void bootstrap(@NonNull Application application, @NonNull NexenServiceInterface callbacks, Options options,
                                       String appName, String appPassword) {

        if (mHasBeenBootstrapped) {
            return;
        }

        mOptions = options;
        mProximityManager = NexenProximityManager.get();
        NexenProximitySetup setup = new NexenProximitySetup(appName, appPassword);
        if (options.useForegroundScanning()) {
          setup.setUseForegroundScanning(true);

          String contentTitle = options.getForegroundScanningOptions().getContentTitle();
          String stopText = options.getForegroundScanningOptions().getStopTitle();
          int iconRes = mOptions.getNotificationOptions().getIconRes(application.getApplicationContext());

          setup.setForegroundNotification(NexenNotificationBuilder.createForegroundNotification(application,
            PushHandlerActivity.class, contentTitle, iconRes, stopText));
        }

        mProximityManager.configureFor(application, callbacks, setup);

        mHasBeenBootstrapped = true;
    }

}